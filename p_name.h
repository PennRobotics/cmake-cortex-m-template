#include "p_name.h"

/* TODO: timer and GPIO macros */

#define  LED_GPIO  *(volatile uint32_t *)(0x40087654)  /* TODO */
#define  LED_MASK  (1<<5)  /* TODO */

void init_gpio();
void init_timer();
void toggle_led();
void delay_ms(int ms);
