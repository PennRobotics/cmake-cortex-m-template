CMake Template for Cortex-M Targets
===================================
_for quick bootstrapping of microcontroller projects, ideally_


## Checklist

- [ ] Create minimum working demo on one specific target
- [ ] [Project integrations](https://gitlab.com/PennRobotics/cmake-cortex-m-template/-/settings/integrations)
- [ ] [CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Security tests (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
- [ ] Use an image to cross compile and create output as artifacts&mdash;ideally on a GitLab Runner
- [ ] Generalize to other targets


## Usage

  <mark>TODO</mark>
