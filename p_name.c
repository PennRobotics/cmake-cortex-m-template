#include "p_name.h"

int main() {
    init_gpio();
    init_timer();
    while (1) {
        toggle_led();
        delay_ms(1000);
    }
    
    for(;;){}  /* End of program; infinite loop */
}


void init_gpio()
{
    /* TODO */
}


void init_timer()
{
    /* TODO */
}


void toggle_led()
{
    LED_GPIO ^= LED_MASK;
}


void delay_ms(int ms)
{
    /* TODO */
}
